# LunaOS Wallpapers

This repository contains all the wallpapers that are used in LunaOS

## This repository uses wallpapers from

- [Linux Dynamic Wallpapers GitHub](https://github.com/saint-13/Linux_Dynamic_Wallpapers)
- [Nordic Wallpapers GitHub](https://github.com/linuxdotexe/nordic-wallpapers)
- [Otto KDE Theme Gitlab](https://gitlab.com/jomada/otto/-/raw/e554dba1147ac377cd2bcc7877a0ee356836f4a5/wallpaper/Otto.png)
- [Dynamic Wallpaper Club](https://dynamicwallpaper.club/)
